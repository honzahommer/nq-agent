NodeQuery Agent
===============

The NodeQuery agent collects selected linux server data and sends them to
our monitoring system for further processing. All data being collected
are listed below.

Requirements
------------

Most major distributions will already include following requirements.

* coreutils
* crontab
* wget
* ip

Tested Distributions (x64)
--------------------------

* CentOS 7
* CentOS 6 (6.0, 6.5)
* CentOS 5 (5.1, 5.8)
* Ubuntu 14.04 LTS
* Ubuntu 12.04 LTS
* Ubuntu 10.04 LTS
* Debian 7 (7.0, 7.5)
* Debian 6
* Arch Linux (2013.05, 2014.04)
* Gentoo (2013.11)
* Slackware (14.1)
* Fedora 20

Monitored Data
--------------

* Agent version
* System uptime
* Session count
* Process count
* Process array
* File handle count
* File handle limit
* OS kernel
* OS name
* OS architecture
* CPU identifier
* CPU cores
* CPU frequency
* RAM total
* RAM usage
* SWAP total
* SWAP usage
* Disk array
* Disk total
* Disk usage
* Connection count
* NIC identifier
* IPv4 address
* IPv6 address
* RX since boot
* TX since boot
* RX currently
* TX currently
* System load
* CPU load
* IO load
* Ping Europe
* Ping USA
* Ping Asia
